+++
title = "Μία"
date = "2017-10-04"
tags = [ "πρόβατα", "μια", "προβατινοταυτότητες" ]
topics = [ "" ]
+++

- [Στοιχεία](#org06a1127)
          - [Ημερολόγιο](#org9ec8106)


<a id="org06a1127"></a>

# Στοιχεία

-   Φύλο: Θηλυκό
-   Γένος: Μποστανίδα
-   Όνομα Μητρός: Λιάρα
-   Όνομα Πατρός: Κυριάκος
-   Ημερομηνία Γέννησης: 01 Μαρτίου 2014
-   Τόπος γέννησης: μποστάνι
-   Δεύτερη γέννα, δίδυμη με τον Σεβαστιανό Σούβλα


<a id="org9ec8106"></a>

# Ημερολόγιο

-   <span class="timestamp-wrapper"><span class="timestamp">[2017-10-07 Sat] </span></span> H Μία γέννησε ένα λιάρο θηλυκό και υιοθέτησε το αδύναμο αρωί απο τα τρία της μάνας της της Λιάρας
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-12-01 Thu] </span></span> Γέννησε ένα αρσενικό. Η Χιονόμπαλλα
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-11-08 Tue] </span></span> 4ml Σελήνιο και εμβόλιο για την εντεροτοξιναιμία
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-07-05 Tue] </span></span> Μαρκάλο με τον Μ.Χιώτη. Αναμένουμε: <span class="timestamp-wrapper"><span class="timestamp">[2016-12-05 Mon]</span></span>
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-03-29 Tue] </span></span> Η Μια έχει τυμπανισμό
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-02-07 Sun] </span></span> Γέννησε σήμρα η Μια ένα υγιέστατο αρσενικό, έχει ρώγα σαν τη μάνα της.
-   160115: - Η Μία σημάδεψε (δεν την πρόλαβε τελικά ο Σάββας αλλα ο Χιώτης)
-   150907: - Η Μία σημάδεψε (πρόλαβε και την έλαβε ο Σάββας)
-   150417: - Λοιμώδες έκθυμα
-   141015: - Η Μια είναι η αγαπημένη προβατίνα του ΠαυλοΜπέζα
-   140301: - Γέννηση
