+++
title = "Αναρί"
date = "2017-10-04"
tags = [ "πρόβατα", "αναρί", "προβατινοταυτότητες" ]
topics = [ "" ]
+++

<div class="org-center">
<div class="HTML">
<a href="<https://picasaweb.google.com/lh/photo/1eCpIFc39C-auOwVcceoW9MTjNZETYmyPJy0liipFm0?feat=embedwebsite>"><img src="![img](https://lh3.googleusercontent.com/-RjnxkHbFQ4U/V5kC-6AMxAI/AAAAAAAAD4w/ixsqHWttNKg4F3JV3fE9s8qlckFv9mCZgCCo/s800/anari.jpg)" height="600" width="600" /></a>

</div>
</div>

-   Όνομα Μητρός: [Μεγάλη Τρίτη]({lisp}(ob:link-to-post (ob:get-post-by-title "Μεγάλη Τρίτη")){/lisp})
-   Όνομα Πατρός: [Μανώλης Χιώτης]({lisp}(ob:link-to-post (ob:get-post-by-title "Μανώλης Χιώτης")){/lisp})
-   Ημερομηνία Γέννησης: <span class="timestamp-wrapper"><span class="timestamp">[2016-04-15 Fri]</span></span>
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-11-08 Tue] </span></span> 4ml Σελήνιο και εμβόλιο για την εντεροτοξιναιμία
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-07-27 Wed]</span></span>: Αδιάθετη η αγαπημένη μας Αναρί. Της χορηγήθηκαν 210 ml Oxytetracycline ενδομυικά στην ουρά, ένα χάπι Albedazole, ένα depon και μια σόδα, και ο Θεός βοηθός που λένε.
