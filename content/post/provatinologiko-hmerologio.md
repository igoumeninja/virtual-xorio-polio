+++
title = "Προβατινολογικό Ημερολόγιο"
date = "2017-10-04"
tags = [ "πρόβατα" ]
topics = [ "" ]
+++

-   <span class="timestamp-wrapper"><span class="timestamp">[2017-12-23 Sat] </span></span> Σφαγή του Κυριάκου (γιός Κάνας)
-   <span class="timestamp-wrapper"><span class="timestamp">[2017-12-07 Thu] </span></span> Αποκόπηκαν τα αρνιά
-   <span class="timestamp-wrapper"><span class="timestamp">[2017-12-04 Mon] </span></span> 2 ml Σελίνιο, 1 ml Βιταμίνες, 2 ml Panclostil εμβόλιο
-   <span class="timestamp-wrapper"><span class="timestamp">[2017-09-18 Mon] </span></span> Εμβολιασμός για εντεροτοξιναιμία απο τον Γιάννη Αναστασιάδη
-   <span class="timestamp-wrapper"><span class="timestamp">[2017-03-28 Tue]</span></span>
    -   Panclostil απο 2 ml σε όλα (άνω των 21 ημερών)
    -   Panacour στα μικρά που αποκόψανε
-   <span class="timestamp-wrapper"><span class="timestamp">[2017-01-14 Sat] </span></span> Εμβόλιο στα αρνιά για εντεροτοξιναιμία
-   <span class="timestamp-wrapper"><span class="timestamp">[2017-01-13 Fri] </span></span> Ξέκομα των αρνιών
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-06-11 Sat] </span></span> Ο Μανώλης έχει κάυλες και κυνηγάει την Μία
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-06-04 Sat] </span></span> Ύστερα απο συνάντηση με Κλή αύξηση σιτηρέσιο σε 4 τεντζερέδια την ημέρα (2&2)
