+++
title = "Κάνα"
date = "2017-10-04"
tags = [ "πρόβατα", "κάνα", "προβατινοταυτότητες" ]
topics = [ "" ]
+++

-   Φύλο: Θηλυκό
-   Γένος: Γερμανίδα, απόγονος απο κριάρι του Βενιζέλου του παλιού
-   Ημερομηνία Γέννησης: Φλεβάρης 2012
-   Τόπος γέννησης: μποστάνι

**Ημερολόγιο**

-   <span class="timestamp-wrapper"><span class="timestamp">[2016-12-05 Mon] </span></span> Η Κάνα γέννησε ένα λιάρομαυρο θηλυκό την Τσούκου Κιουτσούκου
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-11-08 Tue] </span></span> 4ml Σελήνιο και εμβόλιο για την εντεροτοξιναιμία
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-07-07 Thu] </span></span> Η Κάνα μαρκαλιέται με τον Μανώλη οτν Χιώτη <span class="timestamp-wrapper"><span class="timestamp">[2016-12-07 Wed]</span></span>
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-06-02 Thu] </span></span> Την προσεγγίζει ο [Μανώλης Χιώτης]({lisp}(ob:link-to-post (ob:get-post-by-title "Μανώλης Χιώτης")){/lisp})
-   <span class="timestamp-wrapper"><span class="timestamp">[2016-03-16 Wed]</span></span>160216: Η Κάνα γεννάει δυο πολύ όμορφα ασπρόμαυρα προβατάκια (΄να κι ένα). Την Κιουτσούκο και τον Ζορό.
-   <span class="timestamp-wrapper"><span class="timestamp">[2015-09-20 Sun]</span></span>: Ο Χιώτης μαρκάλησε τη Kάνα και έχουμε πιθανή ημερομηνία γενήσεως 17-02-2016
-   <span class="timestamp-wrapper"><span class="timestamp">[2015-05-29 Fri]</span></span>: Η Κάνα γεννάει ένα πολύ τσακαλάκι θηλυκό.
-   <span class="timestamp-wrapper"><span class="timestamp">[2015-01-02 Fri]</span></span>: Με το νεο έτος η Κάνα ζήταγε και το κριάρι μαστερ-Λιάκου το μπούσκο με τα κέρατα πήδηξε φράχτες για την ικανοποιήσει.(Λιάκο αστον παιχταρά κανα δυο μέρες ακόμη γιατί σέρνονται και οι άλλες).
-   <span class="timestamp-wrapper"><span class="timestamp">[2014-12-16 Tue]</span></span>: Η Κάνα κούτσαινε απο το μπροστά αριστερό πόδι.
-   <span class="timestamp-wrapper"><span class="timestamp">[2014-03-14 Fri]</span></span>: Γεννάει τον Σάββα
-   <span class="timestamp-wrapper"><span class="timestamp">[2014-02-19 Wed]</span></span>: Έπειτα απο προβατινολογική εξέταση του δόκτορα Jimmy, βρέθηκε έγκυος στον 3ο προς 4ο μήνα.
-   <span class="timestamp-wrapper"><span class="timestamp">[2013-04-15 Mon]</span></span>: Κάνα Μάνα στον Κυριάκο
-   <span class="timestamp-wrapper"><span class="timestamp">[2012-02-01 Wed]</span></span>: Γέννηση

**Φωτογραφίες**

![img](http://i.imgur.com/LM9k94s.jpg)

![img](http://i.imgur.com/bELEPDh.jpg)

![img](http://i.imgur.com/1ar7yDZ.jpg)

![img](http://i.imgur.com/zUMYG0R.jpg)

![img](http://i.imgur.com/jW6MvWV.jpg)

![img](http://i.imgur.com/JSNcl3O.jpg)

![img](http://i.imgur.com/roc8yT9.jpg)

![img](http://i.imgur.com/Hfciz6h.jpg)
